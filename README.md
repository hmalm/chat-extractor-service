# Dependencies

- Java 1.8+
  - downloads / install instructions from Sun's website:
  http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
- Gradle
  - if using sdkman you can issue
  ```sdk install gradle 4.5```
  - manually, you can follow the instructions at the Gradle website:
   https://docs.gradle.org/current/userguide/installation.html

# Building

In order to build and execute the service from your local environment
you can issue the following Gradle commands.

build and test:
```
gradle clean build
```

to run the service:
```
gradle startService
```

To debug the service from intellij you can setup a 'Remote' Configuration
on your task dropdown.  You will need to add the classpath for main,
and add and name it as a task.  After that you can run the following
Gradle command and enter debugging by pressing the debug icon on that
Remote task you just configured (other IDEs are similar just insure
they are set to the correct debugging port and pointing local):
```
gradle startServiceDebug
```

Keep in mind that debugging may cause errors with breakpoints set due to
timeouts of asynchronous events.

# Design decisions

With only a few hours to build this project, my goals where to try
and keep things simple, use as many conveniences as possible to get up
and running, and try and get as much done in the time-frame as possible.

TODOs are sprinkled through the code in some places where if I had more
time I would shore things up, or in order to point out potential pitfalls
that would need to be addressed in the future.

Given more time, I would bring in vertx-unit to further test
full service functional test cases, and I'd probably bring in jSoup or a
similar html library to parse the html instead of regexp'ing it.

I do think this would have some scaling issues when people point at links
that are overly large to read over http, so I'd probably look into chunking
the data down instead of sucking it all into one String (which I did for
convenience and expediency).

## The good and bad of the framework:
Vertx documentation is located at: http://vertx.io/docs/ in case you're unfamiliar.
The main reason I chose this framework was for ease of getting off the ground,
as it has quick and dirty microservice capabilities, a decent asynchronous development
model, and I've been using it recently so it's familiar.  One great thing about Vertx
is that it's more a composition of libraries than a framework, which I also considered
a positive.   On the negative side, it's easy to shoot yourself in the foot and end up
debugging asynchronous workflow issues with odd cases of Runtime exceptions.  Also, it's
Futures are not compatible with Javas Asynchronous libraries (but will work until they don't),
this is due to Java using the Fork/Join pool and Vertx rolling their own resulting in
possibility of being on an unexpected thread and never completing, or completing with no value
(I have learned that the hard way).  So while that's a pretty big downside it's easy enough
to avoid


# How to query the service

With your favorite REST client of course!
Mine is curl, so here's some example requests:
```bash
curl -XPOST 'http://127.0.0.1:8080/chat' -d '(happy) great to (see) @bob @dick and @jane'
```

```bash
curl -XPOST 'http://127.0.0.1:8080/chat' -d 'Time to (read) some http://slashdot.org'
```

## API

Chat messages data extraction:

Currently extracts emoticons, links and their titles, and mentions.

```
Method: POST
ROUTE: /chat
Post Body: text
Returns: json
```
This api call expects your chat message as the POST body text like the examples listed above
in 'How to query the service'




