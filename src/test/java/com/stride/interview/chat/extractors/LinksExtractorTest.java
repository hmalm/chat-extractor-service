package com.stride.interview.chat.extractors;

import com.stride.interview.chat.fetcher.UrlTitleFetcher;
import com.stride.interview.chat.model.Link;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonObject;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.Assert.*;


public class LinksExtractorTest {
    Vertx vertx = Vertx.vertx();
    class MockURLTitleFetcher extends UrlTitleFetcher {
        public MockURLTitleFetcher(Vertx vertx, HttpClientOptions options) {
            super(vertx, options);
        }
        @Override
        public void resolveTitle(java.lang.String url, Future<String> titleFuture) {
            titleFuture.complete("");
        }
    }

    @Test
    public void testExtract() throws Exception {
        LinksExtractor extractor = new LinksExtractor(new MockURLTitleFetcher(vertx, new HttpClientOptions()));
        String positive1 = "Olympics are starting soon; http://www.nbcolympics.com";
        String positive2 = "My favorite site is https://www.reddit.com/ what's yours?";
        String positive3 = "Yo http://io9.com/ is on http://craigslist.com it's cheap! Just like http://blizzard.com";
        String negative1 = "I say www.google.com sometimes, but its not the full link, right?  Maybe ask the product manager?";
        String negative2 = "How is my link, Yo!";
        String negative3 = " http protocol has the best : in the // world";
        // TODO: actually do domain validation on things as well and fix the hairy weird cases like these:
        String complex1 = "http://http://http://nodomainvalidationyetisbadMMMMKKKAY";
        String complex2 = "http://is the way";   // ...

        assertTrue(linkUrlCheck(Arrays.asList("http://www.nbcolympics.com"), extractor.extract(positive1)));
        assertTrue(linkUrlCheck(Arrays.asList("https://www.reddit.com/"), extractor.extract(positive2)));
        assertTrue(linkUrlCheck(Arrays.asList("http://io9.com/", "http://craigslist.com", "http://blizzard.com"), extractor.extract(positive3)));
        assertNotNull(extractor.extract(negative1));
        assertTrue(extractor.extract(negative1).isEmpty());
        assertTrue(extractor.extract(negative2).isEmpty());
        assertTrue(extractor.extract(negative3).isEmpty());
        // more complex tests later
    }

    @Test
    public void testTitleExtraction() throws Exception {
        // TODO: test that we can fetch a title by standing up a web Verticle and going
        // over localhost to a mapped html page or opening a socket and listening on a port
        // and feeding the contents of a file to the test.
        LinksExtractor extractor = new LinksExtractor(new UrlTitleFetcher(vertx, new HttpClientOptions()));
        String positive1 = "Olympics are starting soon; http://www.nbcolympics.com";
        List<Link> links = extractor.extract(positive1);
        Future<Void> f = Future.future();
        JsonObject testJson = new JsonObject();
        Future<Void> done = Future.future();
        CountDownLatch latch = new CountDownLatch(1);
        extractor.enrichLinksWithTitleData(testJson, links, done);
        done.setHandler(h -> {
            assertTrue(testJson.containsKey("links"));
            assertFalse(testJson.getJsonArray("links").isEmpty());
            assertTrue(testJson.getJsonArray("links")
                    .getJsonObject(0)
                    .getString("url")
                    .equals("http://www.nbcolympics.com"));
            assertFalse(testJson.getJsonArray("links")
                    .getJsonObject(0)
                    .getString("title").isEmpty());
            latch.countDown();
        });
        while (latch.getCount() > 0) {
            latch.await();
        }
    }

    private boolean linkUrlCheck(List<String> links, List<Link> results) {
        for (int i = 0; i < results.size(); i++) {
            if (results.get(i) == null) {
                return false;
            }
            if (links.get(i) == null || !links.get(i).equals(results.get(i).getUrl())) {
                return false;
            }
        }
        return links.size() == results.size();
    }

    @Test
    public void enrichWithLinks() {
        LinksExtractor links = new LinksExtractor(new UrlTitleFetcher(vertx, new HttpClientOptions()));
        JsonObject testJson = new JsonObject();
        List<Link> testLinks = Arrays.asList(new Link("http://slashdot.org")); //, new Link("http://google.com/"));
        AtomicBoolean done = new AtomicBoolean(false);
        Future<Void> completed = Future.future();
        links.enrichLinksWithTitleData(testJson, testLinks, completed);
        completed.setHandler(h -> {
            assertTrue(h.succeeded());
            assertTrue(testJson.containsKey("links"));
            System.out.println("json has links "+testJson);
            assertTrue(!testJson.getJsonArray("links").getJsonObject(0).getString("title").isEmpty());
            done.set(true);
        });
        while(!done.get()) {
            try {
                Thread.sleep(300L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}