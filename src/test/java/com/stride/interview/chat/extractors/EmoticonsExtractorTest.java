package com.stride.interview.chat.extractors;

import org.junit.Test;

import java.util.Arrays;

import static com.stride.interview.chat.extractors.TestUtils.*;
import static org.junit.Assert.*;


public class EmoticonsExtractorTest {

    @Test
    public void testExtract() throws Exception {
        EmoticonsExtractor extractor = new EmoticonsExtractor();
        String positive1 = "Good morning! (megusta)";
        String positive2 = "(boom) it's (candycorn)";
        String positive3 = "(omg) last night was bad, (toilet) (volcano)";
        String negative1 = "(yourthebestmanletmetellyou)";
        String negative2 = "() it's just parens";
        String negative3 = "what the!";
        String complex = "(()) ((())) (((()))) lisp is fun";

        assertTrue(listHasAll(Arrays.asList("megusta"), extractor.extract(positive1)));
        assertTrue(listHasAll(Arrays.asList("boom", "candycorn"), extractor.extract(positive2)));
        assertTrue(listHasAll(Arrays.asList("omg", "toilet", "volcano"), extractor.extract(positive3)));
        assertNotNull(extractor.extract(null));
        assertTrue(extractor.extract(negative1).isEmpty());
        assertTrue(extractor.extract(negative2).isEmpty());
        assertTrue(extractor.extract(negative3).isEmpty());
        assertTrue(listHasAll(Arrays.asList("()", "(())", "((()))"), extractor.extract(complex)));
    }
}