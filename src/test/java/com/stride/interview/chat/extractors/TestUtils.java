package com.stride.interview.chat.extractors;


import com.stride.interview.chat.model.Link;
import io.vertx.core.Future;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

public class TestUtils {

    // may be inefficient but works as a convenience for our tests
    public static boolean listHasAll(List<String> l1, List<String> l2) {
        return l1.stream().filter(l2::contains).count() == l2.size() &&
                l2.stream().filter(l1::contains).count() == l1.size();
    }
    public static List<Link> awaitLinkFutureList(Future<List<Link>> f) {
        final List<Link>[] returnList = new List[1];
        CountDownLatch latch = new CountDownLatch(1);
        f.setHandler(h -> {
            if (h.succeeded()) {
                returnList[0] = h.result();
            } else {
                h.cause().printStackTrace();
                returnList[0] = null;
            }
            latch.countDown();
        });
        while (latch.getCount() > 0) {
            try {
                latch.await();
            } catch (InterruptedException ie) { }
        }
        return returnList[0];
    }

    public static List<String> awaitFutureList(Future<List<String>> f) {
        final List<String>[] returnList = new List[1];
        CountDownLatch latch = new CountDownLatch(1);
        f.setHandler(h -> {
            if (h.succeeded()) {
                returnList[0] = h.result();
            } else {
                h.cause().printStackTrace();
                returnList[0] = null;
            }
            latch.countDown();
        });
        while (latch.getCount() > 0) {
            try {
                latch.await();
            } catch (InterruptedException ie) { }
        }
        return returnList[0];
    }
 }
