package com.stride.interview.chat.extractors;

import org.junit.Test;

import java.util.Arrays;

import static com.stride.interview.chat.extractors.TestUtils.awaitFutureList;
import static org.junit.Assert.*;


public class MentionsExtractorTest {

    @Test
    public void testExtract() throws Exception {
        MentionsExtractor extractor = new MentionsExtractor();
        String positive1 = "@chris you around?";
        // Assumption here that punctuation could be in names and the only seperator is a space!!
        String positive2 = "Hey @bob @jane how are things?";
        String positive3 = "It's great to see @bob @dick and @jane .";
        String negative1 = "Meet me @ the postoffice on Wed.";
        String negative2 = "@ q @";
        String complex = "@@ @@@@ @@@";

        assertTrue(TestUtils.listHasAll(Arrays.asList("chris"), extractor.extract(positive1)));
        assertTrue(TestUtils.listHasAll(Arrays.asList("bob", "jane"), extractor.extract(positive2)));
        assertTrue(TestUtils.listHasAll(Arrays.asList("bob", "dick", "jane"), extractor.extract(positive3)));
        assertTrue(extractor.extract(negative1).isEmpty());
        assertTrue(extractor.extract(negative2).isEmpty());
        assertTrue(TestUtils.listHasAll(Arrays.asList("@", "@@@", "@@"), extractor.extract(complex)));
        assertNotNull(extractor.extract(null));
        assertTrue(extractor.extract(null).isEmpty());
    }


}