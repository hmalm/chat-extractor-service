package com.stride.interview.chat.extractors;


import com.stride.interview.chat.model.Link;
import io.vertx.core.*;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.*;

/**
 * Processes a message for values that need extracting.
 */
public class MessageAnalyzer {
    private static final Logger log = LoggerFactory.getLogger(MessageAnalyzer.class);
    // Done for expediancy, this could be broken out to be more configurable
    private EmoticonsExtractor emoticonsExtractor;
    private LinksExtractor linksExtractor;
    private MentionsExtractor mentionsExtractor;

    public MessageAnalyzer(Vertx vertx, LinksExtractor linksExtractor, MentionsExtractor mentionsExtractor, EmoticonsExtractor emoticonsExtractor) {
        this.emoticonsExtractor = emoticonsExtractor;
        this.linksExtractor = linksExtractor;
        this.mentionsExtractor = mentionsExtractor;
    }

    public void processMessage(String message, Future<JsonObject> resultFuture) {
        Future<Void> futureLinks = Future.future();
        JsonObject json = new JsonObject();
        addToJsonObject(json, "mentions", mentionsExtractor.extract(message));
        addToJsonObject(json, "emoticons", emoticonsExtractor.extract(message));

        List<Link> links = linksExtractor.extract(message);
        linksExtractor.enrichLinksWithTitleData(json, links, futureLinks);

        futureLinks.setHandler(h -> {
            if (h.succeeded()) {
                json.put("links", new JsonArray(links));
            } else {
                log.error("cause was", h.cause());
            }
            resultFuture.complete(json);
        });
    }

    public void addToJsonObject(JsonObject json, String key, List<String> things) {
        if (things != null && things.size() > 0) {
            json.put(key, things);
        }
    }

}
