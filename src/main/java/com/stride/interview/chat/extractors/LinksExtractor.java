package com.stride.interview.chat.extractors;


import com.stride.interview.chat.fetcher.UrlTitleFetcher;
import com.stride.interview.chat.model.Link;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LinksExtractor implements Extractor<Link, String> {
    private static final Logger log = LoggerFactory.getLogger(LinksExtractor.class);
    private static final Pattern linksPattern = Pattern.compile("(https?:\\/\\/[\\S]*)");
    private UrlTitleFetcher urlFetcher;

    public LinksExtractor(UrlTitleFetcher urlFetcher) {
        this.urlFetcher = urlFetcher;
    }

    public Future<String> getFetchedLinkTitle(String url) {
        final Future<String> titleFuture = Future.future();
        urlFetcher.resolveTitle(url, titleFuture);
        return titleFuture;
    }

    /**
     * Pulls url style information out of strings
     *
     * @param s
     * @return
     */
    public List<Link> extract(String s) {
        if (s == null) {
            return new ArrayList<>();
        }
        List<Link> results = new ArrayList<>();
        Matcher m = linksPattern.matcher(s);
        while (m.find()) {
            String url = m.group(1);
            Link link = new Link(url);
            results.add(link);
        }
        return results;
    }

    /**
     * Takes a list of links, a target json to write to and a future that gets completed when the
     * work of adding the links titles are done.
     *
     * @param json
     * @param links
     * @param completionFuture
     */
    public void enrichLinksWithTitleData(JsonObject json, List<Link> links, Future<Void> completionFuture) {
        final List<Future> futures = new ArrayList<>();
        log.info("links objects ["+links.size()+"]");
        links.stream()
                .forEachOrdered(
                        link -> {
                            log.info("link future for "+link.getUrl()+ " should be done now.");
                            Future<String> f = getFetchedLinkTitle(link.getUrl());
                            futures.add(f);
                        });
        log.info("Futures to resolve for links ["+links.size()+"]");
        CompositeFuture.join(futures).setHandler(h -> {
            // insure the futures have fired since we are dealing with all of them.
            if (h.succeeded()) {
                log.info("are we there yet?");
                List<String> results = h.result().list();
                log.info("responses were :"+ String.join(", ", results));
                for (int i = 0; i < results.size(); i++) {
                    links.get(i).setTitle(results.get(i));
                    addTitleToJSon(json, links.get(i));
                }
            }
            completionFuture.complete();
        });
    }

    void addTitleToJSon(JsonObject json, Link link) {
        if (!json.containsKey("links")) {
            json.put("links", new JsonArray());
        }
        json.getJsonArray("links")
                .add(new JsonObject()
                        .put("url", link.getUrl())
                        .put("title", link.getTitle()));
    }

}
