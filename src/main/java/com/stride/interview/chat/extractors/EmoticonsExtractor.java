package com.stride.interview.chat.extractors;

import io.vertx.core.Future;

import java.util.List;
import java.util.regex.Pattern;


public class EmoticonsExtractor implements Extractor<String, String> {
    private static final Pattern emoticonPattern = Pattern.compile("\\((\\S{1,15})\\)");

    @Override
    public List<String> extract(String s) {
        return extractWithPattern(s, emoticonPattern);
    }
}
