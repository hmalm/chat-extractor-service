package com.stride.interview.chat.extractors;


import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface Extractor<T, R> {
    Logger log = LoggerFactory.getLogger(Extractor.class);

    List<T> extract(R r);

    default List<String> extractWithPattern(String s, Pattern p) {
        List<String> extracted = new ArrayList<>();
        if (s == null) {
            return extracted;
        }
        Matcher m = p.matcher(s);
        while (m.find()) {
            extracted.add(m.group(1));
        }
        return extracted;
    }

}
