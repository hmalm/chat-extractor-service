package com.stride.interview.chat.extractors;

import io.vertx.core.Future;

import java.util.List;
import java.util.regex.Pattern;


public class MentionsExtractor implements Extractor<String, String> {
    private static final Pattern mentionPattern = Pattern.compile("@(\\S+)");

    @Override
    public List<String> extract(String s) {
        return extractWithPattern(s, mentionPattern);
    }
}
