package com.stride.interview.chat;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.stride.interview.chat.extractors.EmoticonsExtractor;
import com.stride.interview.chat.extractors.LinksExtractor;
import com.stride.interview.chat.extractors.MentionsExtractor;
import com.stride.interview.chat.extractors.MessageAnalyzer;
import com.stride.interview.chat.fetcher.UrlTitleFetcher;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * ChatService is used for analyzing posted chat messages and returning metadata contained
 * within the message.
 */
public class ChatService extends AbstractVerticle {
    private static final Logger log = LoggerFactory.getLogger(ChatService.class);
    private static final String JSON_ERROR_PROPERTY_NAME = "Error";
    private static final int DEFAULT_PORT = 8080;
    public static final String APPLICATION_JSON = "application/json";
    private JsonObject config;
    private static MessageAnalyzer analyzer;

    // Gets started by Vertx lifecycle
    public void start() {
        log.info("starting up " + ChatService.class.getSimpleName());
        initService();
        final Router apiRouter = Router.router(vertx);
        apiRouter.route().handler(BodyHandler.create());
        apiRouter.post("/chat")
                .produces(APPLICATION_JSON)
                .handler(this::handleChatMesage);
        int port = config.getInteger("port", DEFAULT_PORT);
        vertx.createHttpServer()
                .requestHandler(apiRouter::accept).listen(port);
        log.info("server is starting on port " + port);
    }

    public void initService() {
        setJsonOptions();
        // pick up our vertx configuration
        log.info("getting configuration");
        config = (config() == null) ? new JsonObject() : config();
        if (config == null) {
            config = new JsonObject();
        }
        log.info("registering analyzers");
        analyzer = new MessageAnalyzer(vertx,
                new LinksExtractor(new UrlTitleFetcher(vertx, new HttpClientOptions(config.getJsonObject("client")))),
                new MentionsExtractor(),
                new EmoticonsExtractor());
    }

    /**
     * Vertx relies on jackson under the covers, so we can override default serialization of JSON.
     */
    public static void setJsonOptions() {
        Json.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Json.mapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
    }

    /**
     * Handler for the chat route.
     *
     * @param routingContext
     */
    private void handleChatMesage(RoutingContext routingContext) {
        log.info("handling request for chat message.");
        try {
            String message = routingContext.getBodyAsString("UTF-8");
            Future<JsonObject> jsonObjectFuture = Future.future();

            analyzer.processMessage(message, jsonObjectFuture);
            jsonObjectFuture.setHandler(done -> {
                if (done.succeeded()) {
                    routingContext.response()
                            .setStatusCode(200)
                            .putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON)
                            .end(done.result().encodePrettily());
                } else {
                    String err = (done.cause() != null && done.cause().getMessage() != null) ? done.cause().getMessage() : "Unknown Error";
                    serviceError(routingContext, "Error processing message: " + err);
                }
            });

        } catch (DecodeException dce) {
            dce.printStackTrace(); // TODO: cleaner logging of errors
            serviceError(routingContext, "Invalid request, the message was malformed!");
        } catch (Exception e) {
            e.printStackTrace(); // same as above.. cleanup
            unknownError(routingContext, "Unexpected failure: " + ((e == null) ? "Unknown" : e.getMessage()));
        }
    }

    /**
     * Used for handling http service errors in a generic fashion.
     *
     * @param ctx
     * @param messge
     */
    public static void serviceError(RoutingContext ctx, String messge) {
        ctx.response()
                .setStatusCode(400)
                .putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON)
                .end(
                        new JsonObject()
                                .put(JSON_ERROR_PROPERTY_NAME, messge)
                                .encodePrettily()
                );
    }

    /**
     * Used for handling failures for http
     *
     * @param ctx
     * @param message
     */
    public static void unknownError(RoutingContext ctx, String message) {
        ctx.response()
                .setStatusCode(500)
                .putHeader(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON)
                .end(
                        new JsonObject()
                                .put(JSON_ERROR_PROPERTY_NAME, message)
                                .encodePrettily()
                );
    }

    public static final void main(String[] args) throws IOException {
        Vertx vertx = Vertx.vertx();
        JsonObject conf = new JsonObject(new String(Files.readAllBytes(Paths.get("src/main/resources/config.json")), Charset.defaultCharset()));
        DeploymentOptions opts = new DeploymentOptions().setConfig(conf).setInstances(1).setWorker(true);
        vertx.deployVerticle("com.stride.interview.chat.ChatService", opts);
    }
}