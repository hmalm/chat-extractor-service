package com.stride.interview.chat.fetcher;


import com.stride.interview.chat.model.Link;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Used to get the html title of a link
 */
public class UrlTitleFetcher {
    private static final Logger log = LoggerFactory.getLogger(UrlTitleFetcher.class);
    // TODO: This is a naive solution as it doesn't take into consideration comments or data content, javascript, etc)
    // also we may want to evaluate a priority of importance of different web standards that spell out title in meta tags
    // TODO: pull in jsoup or similar html consuming library to actually tear apart the structure of the document when there is more time allowed
    private static final Pattern title = Pattern.compile("<title>(.*)?<\\/title", Pattern.MULTILINE|Pattern.CASE_INSENSITIVE);
    //TODO: if we can fall back to something like this when 'title' pattern not found
    private static final Pattern fallbackOGTitle = Pattern.compile("<meta\\s+?property=\"og:title\"\\s+?content=\"(.*)\"");
    //TODO: insure https support through options and 301 redirect following
    HttpClient client;

    public UrlTitleFetcher(Vertx vertx, HttpClientOptions options) {
        client = vertx.createHttpClient(options);
    }

    /**
     * Does the work of crawling for an html pages title
     * Uses title tag as first choice and defaults to meta og:properties (TODO: currently untested for og)
     *
     * @param url
     * @param titleFuture
     */
    public void resolveTitle(String url, final Future<String> titleFuture) {
            client.getAbs(url)
                    .setFollowRedirects(true)
                    .setTimeout(5000L)
                    .exceptionHandler(clientExceptionHandler(url, titleFuture))
                    .handler(response -> {
                        if (response.statusCode() != 200) {
                            log.error("tried to fetch "+url+" but got a "+response.statusCode()+":"+response.statusMessage());
                            titleFuture.complete("");
                        } else {
                            response.bodyHandler(handleCrawlBody(url, titleFuture));
                        }
                    }).end();
    }

    private Handler<Throwable> clientExceptionHandler(String url, Future<String> titleFuture) {
        return ex -> {
            ex.printStackTrace();
            log.error("failed to content url for " + url, ex);
            titleFuture.complete("");
        };
    }

    private Handler<Buffer> handleCrawlBody(String url, Future<String> titleFuture) {
        return bodyH -> {
            // TODO: fix handling potential huge buffers by using chunking if we can't scale memory-wise
            String body = bodyH.toString("UTF-8");
            log.debug("body was >> "+body);
            Matcher m = title.matcher(body);
            if (m.find()) { // only take the first title
                String title = m.group(1);
                log.info("found title for "+url+ " : "+title);
                titleFuture.complete(title);
            } else {
                Matcher mog = fallbackOGTitle.matcher(body);
                if (mog.find()) {
                    String title = m.group(1);
                    log.info("found title for "+url+" from title meta property "+title);
                    titleFuture.complete(title);
                } else {
                    log.info("couldn't find title for " + url);
                    titleFuture.complete("");
                }
            }
        };
    }
}
