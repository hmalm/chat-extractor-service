package com.stride.interview.chat.model;

/**
 * Convenience Object for link data.
 */
public class Link {
    private String url;
    private String title;

    public Link() { }

    public Link(String title, String url) {
        this.title = title;
        this.url = url;
    }

    public Link(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Link{" +
                "url='" + url + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
